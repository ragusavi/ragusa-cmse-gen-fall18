#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "CL/opencl.h"
#include "AOCLUtils/aocl_utils.h"

using namespace aocl_utils;

// OpenCL runtime configuration
cl_platform_id platform = NULL;
unsigned num_devices = 0;
scoped_array <cl_device_id> device; // num_devices elements
cl_context context = NULL;
scoped_array <cl_command_queue> queue; // num_devices elements
cl_program program = NULL;
scoped_array <cl_kernel> kernel; // num_devices elements
#if USE_SVM_API == 0
scoped_array <cl_mem> X_buf; // num_devices elements
scoped_array <cl_mem> Y_buf; // num_devices elements
scoped_array <cl_mem> Vx_buf; // num_devices elements
scoped_array <cl_mem> Vy_buf; // num_devices elements
scoped_array <cl_mem> Px_buf; // num_devices elements
scoped_array <cl_mem> Py_buf; // num_devices elements
scoped_array <cl_mem> F_buf; // num_devices elements
scoped_array <cl_mem> Params_buf; // num_devices elements
#endif /* USE_SVM_API == 0 */

// Problem data.
unsigned N = 1000000; // problem size
#if USE_SVM_API == 0
scoped_array <scoped_aligned_ptr<float>> X, Y, Vx, Vy, Px, Py, F, Params; // num_devices elements
#else
scoped_array<scoped_SVM_aligned_ptr<float> >  X, Y, Vx, Vy, Px, Py, F, Params; // num_devices elements
#endif /* USE_SVM_API == 0 */
scoped_array<unsigned> n_per_device; // num_devices elements

// Function prototypes
float rand_float();

bool init_opencl();

void init_problem();

void run();

void cleanup();

// Entry point.
int main(int argc, char **argv) {
    Options options(argc, argv);

    // Optional argument to specify the problem size.
    if (options.has("n")) {
        N = options.get<unsigned>("n");
    }

    // Initialize OpenCL.
    if (!init_opencl()) {
        return -1;
    }

    // Initialize the problem data.
    // Requires the number of devices to be known.
    init_problem();

    // Run the kernel.
    run();

    // Free the resources allocated
    cleanup();

    return 0;
}

/////// HELPER FUNCTIONS ///////

// Randomly generate a floating-point number between -10 and 10.
float rand_float() {
    return float(rand()) / float(RAND_MAX) * 20.0f - 10.0f;
}

// Initializes the OpenCL objects.
bool init_opencl() {
    cl_int status;

    printf("Initializing OpenCL\n");

    if (!setCwdToExeDir()) {
        return false;
    }

    // Get the OpenCL platform.
    platform = findPlatform("Intel");
    if (platform == NULL) {
        printf("ERROR: Unable to find Intel FPGA OpenCL platform.\n");
        return false;
    }

    // Query the available OpenCL device.
    device.reset(getDevices(platform, CL_DEVICE_TYPE_ALL, &num_devices));
    printf("Platform: %s\n", getPlatformName(platform).c_str());
    printf("Using %d device(s)\n", num_devices);
    for (unsigned i = 0; i < num_devices; ++i) {
        printf("  %s\n", getDeviceName(device[i]).c_str());
    }

    // Create the context.
    context = clCreateContext(NULL, num_devices, device, &oclContextCallback, NULL, &status);
    checkError(status, "Failed to create context");

    // Create the program for all device. Use the first device as the
    // representative device (assuming all device are of the same type).
    std::string binary_file = getBoardBinaryFile("vector_add", device[0]);
    printf("Using AOCX: %s\n", binary_file.c_str());
    program = createProgramFromBinary(context, binary_file.c_str(), device, num_devices);

    // Build the program that was just created.
    status = clBuildProgram(program, 0, NULL, "", NULL, NULL);
    checkError(status, "Failed to build program");

    // Create per-device objects.
    queue.reset(num_devices);
    kernel.reset(num_devices);
    n_per_device.reset(num_devices);
#if USE_SVM_API == 0
    X_buf.reset(num_devices);
    Y_buf.reset(num_devices);
    Vx_buf.reset(num_devices);
    Vy_buf.reset(num_devices);
    Px_buf.reset(num_devices);
    Py_buf.reset(num_devices);
    F_buf.reset(num_devices);
    Params_buf.reset(num_devices);
#endif /* USE_SVM_API == 0 */

    for (unsigned i = 0; i < num_devices; ++i) {
        // Command queue.
        queue[i] = clCreateCommandQueue(context, device[i], CL_QUEUE_PROFILING_ENABLE, &status);
        checkError(status, "Failed to create command queue");

        // Kernel.
        const char *kernel_name = "vector_add";
        kernel[i] = clCreateKernel(program, kernel_name, &status);
        checkError(status, "Failed to create kernel");

        // Determine the number of elements processed by this device.
        n_per_device[i] = N / num_devices; // number of elements handled by this device

        // Spread out the remainder of the elements over the first
        // N % num_devices.
        if (i < (N % num_devices)) {
            n_per_device[i]++;
        }

#if USE_SVM_API == 0
        // Input buffers.
        X_buf[i] = clCreateBuffer(context, CL_MEM_READ_ONLY, n_per_device[i] * sizeof(float), NULL, &status);
        checkError(status, "Failed to create buffer for input X");

        Y_buf[i] = clCreateBuffer(context, CL_MEM_READ_ONLY, n_per_device[i] * sizeof(float), NULL, &status);
        checkError(status, "Failed to create buffer for input Y");

        Vx_buf[i] = clCreateBuffer(context, CL_MEM_READ_ONLY, n_per_device[i] * sizeof(float), NULL, &status);
        checkError(status, "Failed to create buffer for Vx");

        Vy_buf[i] = clCreateBuffer(context, CL_MEM_READ_ONLY, n_per_device[i] * sizeof(float), NULL, &status);
        checkError(status, "Failed to create buffer for input Vy");

        Px_buf[i] = clCreateBuffer(context, CL_MEM_READ_ONLY, n_per_device[i] * sizeof(float), NULL, &status);
        checkError(status, "Failed to create buffer for input Px");

        Py_buf[i] = clCreateBuffer(context, CL_MEM_READ_ONLY, n_per_device[i] * sizeof(float), NULL, &status);
        checkError(status, "Failed to create buffer for Py");

        F_buf[i] = clCreateBuffer(context, CL_MEM_WRITE_ONLY, n_per_device[i] * sizeof(float), NULL,
                                  &status); //figure out read/write memory
        checkError(status, "Failed to create buffer for input F");

        Params_buf[i] = clCreateBuffer(context, CL_MEM_READ_ONLY, n_per_device[i] * sizeof(float), NULL, &status);
        checkError(status, "Failed to create buffer for input Params");
#else
        cl_device_svm_capabilities caps = 0;

    status = clGetDeviceInfo(
      device[i],
      CL_DEVICE_SVM_CAPABILITIES,
      sizeof(cl_device_svm_capabilities),
      &caps,
      0
    );
    checkError(status, "Failed to get device info");

    if (!(caps & CL_DEVICE_SVM_COARSE_GRAIN_BUFFER)) {
      printf("The host was compiled with USE_SVM_API, however the device currently being targeted does not support SVM.\n");
      // Free the resources allocated
      cleanup();
      return false;
    }
#endif /* USE_SVM_API == 0 */
    }

    return true;
}

// Initialize the data for the problem. Requires num_devices to be known.
void init_problem() {
    if (num_devices == 0) {
        checkError(-1, "No devices");
    }

    X.reset(num_devices);
    Y.reset(num_devices);
    Vx.reset(num_devices);
    Vy.reset(num_devices);
    Px.reset(num_devices);
    Py.reset(num_devices);
    F.reset(num_devices);
    Params.reset(num_devices);

    // Generate input vectors A and B and the reference output consisting
    // of a total of N elements.
    // We create separate arrays for each device so that each device has an
    // aligned buffer.
    for (unsigned i = 0; i < num_devices; ++i) {
#if USE_SVM_API == 0
        X[i].reset(n_per_device[i]);
        Y[i].reset(n_per_device[i]);
        Vx[i].reset(n_per_device[i]);
        Vy[i].reset(n_per_device[i]);
        Px[i].reset(n_per_device[i]);
        Py[i].reset(n_per_device[i]);
        F[i].reset(n_per_device[i]);
        Params[i].reset(n_per_device[i]);

        for (unsigned j = 0; j < n_per_device[i]; ++j) {
            X[i][j] = rand_float();
            Y[i][j] = rand_float();
            Vx[i][j] = rand_float();
            Vy[i][j] = rand_float();
            Px[i][j] = X[i][j];
            Py[i][j] = Y[i][j];
            F[i][j] = 1e100;
        }
        Params[i][0] = 100; //pop size
        Params[i][1] = 1000; //runtime
        Params[i][2] = 0.65; //w
        Params[i][3] = 2.0; //c1
        Params[i][4] = 2.0; //c2
        Params[i][5] = 2.0; //speed_limit
#else
    X[i].reset(context, n_per_device[i]);
    Y[i].reset(context, n_per_device[i]);
    Vx[i].reset(context, n_per_device[i]);
    Vy[i].reset(context, n_per_device[i]);
    Px[i].reset(context, n_per_device[i]);
    Py[i].reset(context, n_per_device[i]);
    F[i].reset(context, n_per_device[i]);
    Params[i].reset(context, n_per_device[i]);

    cl_int status;

    status = clEnqueueSVMMap(queue[i], CL_TRUE, CL_MAP_WRITE, (void *)X[i], n_per_device[i] * sizeof(float), 0, NULL, NULL);
    checkError(status, "Failed to map input X");
    status = clEnqueueSVMMap(queue[i], CL_TRUE, CL_MAP_WRITE, (void *)Y[i], n_per_device[i] * sizeof(float), 0, NULL, NULL);
    checkError(status, "Failed to map input Y");
    status = clEnqueueSVMMap(queue[i], CL_TRUE, CL_MAP_WRITE, (void *)Vx[i], n_per_device[i] * sizeof(float), 0, NULL, NULL);
    checkError(status, "Failed to map input Vx");
    status = clEnqueueSVMMap(queue[i], CL_TRUE, CL_MAP_WRITE, (void *)Vy[i], n_per_device[i] * sizeof(float), 0, NULL, NULL);
    checkError(status, "Failed to map input Vy");
    status = clEnqueueSVMMap(queue[i], CL_TRUE, CL_MAP_WRITE, (void *)Px[i], n_per_device[i] * sizeof(float), 0, NULL, NULL);
    checkError(status, "Failed to map input Px");
    status = clEnqueueSVMMap(queue[i], CL_TRUE, CL_MAP_WRITE, (void *)Py[i], n_per_device[i] * sizeof(float), 0, NULL, NULL);
    checkError(status, "Failed to map input Py");
    status = clEnqueueSVMMap(queue[i], CL_TRUE, CL_MAP_WRITE, (void *)F[i], n_per_device[i] * sizeof(float), 0, NULL, NULL);
    checkError(status, "Failed to map input F");
    status = clEnqueueSVMMap(queue[i], CL_TRUE, CL_MAP_WRITE, (void *)Params[i], n_per_device[i] * sizeof(float), 0, NULL, NULL);
    checkError(status, "Failed to map input Params");

    for (unsigned j = 0; j < n_per_device[i]; ++j) {
            X[i][j] = rand_float();
            Y[i][j] = rand_float();
            Vx[i][j] = rand_float();
            Vy[i][j] = rand_float();
            Px[i][j] = X[i][j];
            Py[i][j] = Y[i][j];
            F[i][j] = 1e100;
        }
        Params[i][0] = 100; //pop size
        Params[i][1] = 1000; //runtime
        Params[i][2] = 0.65; //w
        Params[i][3] = 2.0; //c1
        Params[i][4] = 2.0; //c2
        Params[i][5] = 2.0; //speed_limit

    status = clEnqueueSVMUnmap(queue[i], (void *)X[i], 0, NULL, NULL);
    checkError(status, "Failed to unmap input X");
    status = clEnqueueSVMUnmap(queue[i], (void *)Y[i], 0, NULL, NULL);
    checkError(status, "Failed to unmap input Y");
    status = clEnqueueSVMUnmap(queue[i], (void *)Vx[i], 0, NULL, NULL);
    checkError(status, "Failed to unmap input Vx");
    status = clEnqueueSVMUnmap(queue[i], (void *)Vy[i], 0, NULL, NULL);
    checkError(status, "Failed to unmap input Vy");
    status = clEnqueueSVMUnmap(queue[i], (void *)Px[i], 0, NULL, NULL);
    checkError(status, "Failed to unmap input Px");
    status = clEnqueueSVMUnmap(queue[i], (void *)Py[i], 0, NULL, NULL);
    checkError(status, "Failed to unmap input Py");
    status = clEnqueueSVMUnmap(queue[i], (void *)F[i], 0, NULL, NULL);
    checkError(status, "Failed to unmap input F");
    status = clEnqueueSVMUnmap(queue[i], (void *)Params[i], 0, NULL, NULL);
    checkError(status, "Failed to unmap input Params");
#endif /* USE_SVM_API == 0 */
    }
}

void run() {
    cl_int status;

    const double start_time = getCurrentTimestamp();

    // Launch the problem for each device.
    scoped_array <cl_event> kernel_event(num_devices);
    scoped_array <cl_event> finish_event(num_devices);

    for (unsigned i = 0; i < num_devices; ++i) {

#if USE_SVM_API == 0
        // Transfer inputs to each device. Each of the host buffers supplied to
        // clEnqueueWriteBuffer here is already aligned to ensure that DMA is used
        // for the host-to-device transfer.
        cl_event write_event[8];
        status = clEnqueueWriteBuffer(queue[i], X_buf[i], CL_FALSE, 0, n_per_device[i] * sizeof(float), X[i], 0, NULL, &write_event[0]);
        checkError(status, "Failed to transfer input X");

        status = clEnqueueWriteBuffer(queue[i], Y_buf[i], CL_FALSE, 0, n_per_device[i] * sizeof(float), Y[i], 0, NULL, &write_event[1]);
        checkError(status, "Failed to transfer input Y");

        status = clEnqueueWriteBuffer(queue[i], Vx_buf[i], CL_FALSE, 0, n_per_device[i] * sizeof(float), Vx[i], 0, NULL, &write_event[2]);
        checkError(status, "Failed to transfer input Vx");

        status = clEnqueueWriteBuffer(queue[i], Vy_buf[i], CL_FALSE, 0, n_per_device[i] * sizeof(float), Vy[i], 0, NULL, &write_event[3]);
        checkError(status, "Failed to transfer input Vy");

        status = clEnqueueWriteBuffer(queue[i], Px_buf[i], CL_FALSE, 0, n_per_device[i] * sizeof(float), Px[i], 0, NULL, &write_event[4]);
        checkError(status, "Failed to transfer input Px");

        status = clEnqueueWriteBuffer(queue[i], Py_buf[i], CL_FALSE, 0, n_per_device[i] * sizeof(float), Py[i], 0, NULL, &write_event[5]);
        checkError(status, "Failed to transfer input Py");

        status = clEnqueueWriteBuffer(queue[i], F_buf[i], CL_FALSE, 0, n_per_device[i] * sizeof(float), F[i], 0, NULL, &write_event[6]);
        checkError(status, "Failed to transfer input F");

        status = clEnqueueWriteBuffer(queue[i], Params_buf[i], CL_FALSE, 0, n_per_device[i] * sizeof(float), Params[i], 0, NULL, &write_event[7]);
        checkError(status, "Failed to transfer input Params");
#endif /* USE_SVM_API == 0 */

        // Set kernel arguments.
        unsigned argi = 0;

#if USE_SVM_API == 0
        status = clSetKernelArg(kernel[i], argi++, sizeof(cl_mem), &X_buf[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArg(kernel[i], argi++, sizeof(cl_mem), &Y_buf[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArg(kernel[i], argi++, sizeof(cl_mem), &Vx_buf[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArg(kernel[i], argi++, sizeof(cl_mem), &Vy_buf[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArg(kernel[i], argi++, sizeof(cl_mem), &Px_buf[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArg(kernel[i], argi++, sizeof(cl_mem), &Py_buf[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArg(kernel[i], argi++, sizeof(cl_mem), &F_buf[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArg(kernel[i], argi++, sizeof(cl_mem), &Params_buf[i]);
        checkError(status, "Failed to set argument %d", argi - 1);
#else
        status = clSetKernelArgSVMPointer(kernel[i], argi++, (void*)X[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArgSVMPointer(kernel[i], argi++, (void*)Y[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArgSVMPointer(kernel[i], argi++, (void*)Vx[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArgSVMPointer(kernel[i], argi++, (void*)Vy[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArgSVMPointer(kernel[i], argi++, (void*)Px[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArgSVMPointer(kernel[i], argi++, (void*)Py[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArgSVMPointer(kernel[i], argi++, (void*)F[i]);
        checkError(status, "Failed to set argument %d", argi - 1);

        status = clSetKernelArgSVMPointer(kernel[i], argi++, (void*)Params[i]);
        checkError(status, "Failed to set argument %d", argi - 1);
#endif /* USE_SVM_API == 0 */

        // Enqueue kernel.
        // Use a global work size corresponding to the number of elements to add
        // for this device.
        //
        // We don't specify a local work size and let the runtime choose
        // (it'll choose to use one work-group with the same size as the global
        // work-size).
        //
        // Events are used to ensure that the kernel is not launched until
        // the writes to the input buffers have completed.
        const size_t global_work_size = n_per_device[i];
        printf("Launching for device %d (%zd elements)\n", i, global_work_size);

#if USE_SVM_API == 0
        status = clEnqueueNDRangeKernel(queue[i], kernel[i], 1, NULL,
                                        &global_work_size, NULL, 2, write_event, &kernel_event[i]);
#else
        status = clEnqueueNDRangeKernel(queue[i], kernel[i], 1, NULL,
        &global_work_size, NULL, 0, NULL, &kernel_event[i]);
#endif /* USE_SVM_API == 0 */
        checkError(status, "Failed to launch kernel");

#if USE_SVM_API == 0
        // Read the result. This the final operation.
        status = clEnqueueReadBuffer(queue[i], F_buf[i], CL_FALSE, 0, n_per_device[i] * sizeof(float), F[i], 1, &kernel_event[i], &finish_event[i]);

        // Release local events.
        clReleaseEvent(write_event[0]);
        clReleaseEvent(write_event[1]);
        clReleaseEvent(write_event[2]);
        clReleaseEvent(write_event[3]);
        clReleaseEvent(write_event[4]);
        clReleaseEvent(write_event[5]);
        clReleaseEvent(write_event[6]);
        clReleaseEvent(write_event[7]);
#else
    status = clEnqueueSVMMap(queue[i], CL_TRUE, CL_MAP_READ, (void *)F[i], n_per_device[i] * sizeof(float), 0, NULL, NULL);
    checkError(status, "Failed to map output");
    clFinish(queue[i]);
#endif /* USE_SVM_API == 0 */
    }

    // Wait for all devices to finish.
    clWaitForEvents(num_devices, finish_event);

    const double end_time = getCurrentTimestamp();

    // Wall-clock time taken.
    printf("\nTime: %0.3f ms\n", (end_time - start_time) * 1e3);

    // Get kernel times using the OpenCL event profiling API.
    for (unsigned i = 0; i < num_devices; ++i) {
        cl_ulong time_ns = getStartEndTime(kernel_event[i]);
        printf("Kernel time (device %d): %0.3f ms\n", i, double(time_ns) * 1e-6);
    }

    // Release all events.
    for (unsigned i = 0; i < num_devices; ++i) {
        clReleaseEvent(kernel_event[i]);
        clReleaseEvent(finish_event[i]);
    }
    /*
    // Verify results.
    bool pass = true;
    for (unsigned i = 0; i < num_devices && pass; ++i) {
        for (unsigned j = 0; j < n_per_device[i] && pass; ++j) {
            if (fabsf(output[i][j] - ref_output[i][j]) > 1.0e-5f) {
                printf("Failed verification @ device %d, index %d\nOutput: %f\nReference: %f\n",
                       i, j, output[i][j], ref_output[i][j]);
                pass = false;
            }
        }
    }
     */

#if USE_SVM_API == 1
    for (unsigned i = 0; i < num_devices; ++i) {
        status = clEnqueueSVMUnmap(queue[i], (void *)F[i], 0, NULL, NULL);
        checkError(status, "Failed to unmap output");
  }
#endif /* USE_SVM_API == 1 */
    printf("\nVerification: %s\n", pass ? "PASS" : "FAIL");
}

// Free the resources allocated during initialization
void cleanup() {
    for (unsigned i = 0; i < num_devices; ++i) {
        if (kernel && kernel[i]) {
            clReleaseKernel(kernel[i]);
        }
        if (queue && queue[i]) {
            clReleaseCommandQueue(queue[i]);
        }
#if USE_SVM_API == 0
        if (X_buf && X_buf[i]) {
            clReleaseMemObject(X_buf[i]);
        }
        if (Y_buf && Y_buf[i]) {
            clReleaseMemObject(Y_buf[i]);
        }
        if (Vx_buf && Vx_buf[i]) {
            clReleaseMemObject(Vx_buf[i]);
        }
        if (Vy_buf && Vy_buf[i]) {
            clReleaseMemObject(Vy_buf[i]);
        }
        if (Px_buf && Vx_buf[i]) {
            clReleaseMemObject(Px_buf[i]);
        }
        if (Py_buf && Py_buf[i]) {
            clReleaseMemObject(Py_buf[i]);
        }
        if (F_buf && F_buf[i]) {
            clReleaseMemObject(F_buf[i]);
        }
        if (Params_buf && Params_buf[i]) {
            clReleaseMemObject(Params_buf[i]);
        }
#else
    if(X[i].get())
      X[i].reset();
    if(Y[i].get())
      Y[i].reset();
    if(Vx[i].get())
      Vx[i].reset();
    if(Vy[i].get())
      Vy[i].reset();
    if(Px[i].get())
      Px[i].reset();
    if(Py[i].get())
      Py[i].reset();
    if(F[i].get())
      F[i].reset();
    if(Params[i].get())
      Params[i].reset();
#endif /* USE_SVM_API == 0 */
    }

    if (program) {
        clReleaseProgram(program);
    }
    if (context) {
        clReleaseContext(context);
    }
}
