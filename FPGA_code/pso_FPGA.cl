__kernel void
pso(float *restrict X, float *restrict Y, float *restrict Vx, float *restrict Vy, float *restrict, float *restrict Px,
    float *restrict Py, float *restrict F, const float *Params) {

    //unpack parameters
    int population_size = int(Params[0]);
    int max_time = int(Params[1]);
    float w = Params[2];
    float c1 = Params[3];
    float c2 = Params[4];
    float speed_limit = Params[5];

    //configure starting global best values
    float Gf = 1e300;
    float Gx = 0;
    float Gy = 0;

    for (int i = 0; i < population_size; i++) {
        F[i] = (X[i] * X[i]) + (Y[i] * Y[i]);
        if (F[i] < Gf) {
            Gf = F[i];
            Gx = X[i];
            Gy = Y[i];
        }
    }

    for (int update = 0; update < max_time; update++) {
        for (int i = 0; i < population_size; i++) {
            // update_velocity and restrict to speed limit
            Vx[i] = w * Vx[i] + c1 * (Px[i] - X[i]) + c2 * (Gx - X[i]);
            if (Vx[i] > max_speed) { Vx[i] = max_speed; }
            if (Vx[i] < (-1) * max_speed) { Vx[i] = (-1) * max_speed; }

            Vy[i] = w * Vy[i] + c1 * (Py[i] - Y[i]) + c2 * (Gy - Y[i]);
            if (Vy[i] > max_speed) { Vy[i] = max_speed; }
            if (Vy[i] < (-1) * max_speed) { Vy[i] = (-1) * max_speed; }

            //update_position
            X[i] = X[i] + Vx[i];
            Y[i] = Y[i] + Vy[i];

            // update personal best
            double
            newFitness = (X[i] * X[i]) + (Y[i] * Y[i])
            if (newFitness < F[i]) {
                Px[i] = X[i];
                Py[i] = Y[i];
            }

            //update fitness
            F[i] = newFitness;

            //update global bests
            if (F[i] < Gf) { //how do you do a reduce in openCL?
                Gf = F[i];
                Gx = X[i];
                Gy = Y[i];
            }
        }
    }
}