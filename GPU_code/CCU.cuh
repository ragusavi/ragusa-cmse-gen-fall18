// nicely wraps cuda memory operations
namespace ccu { // Custom CUda namespace
    // array, with lazy JIT cudaMemcpy-ing between host and device (inspired by HEMI, but HEMI's is broken)
    template<typename T>
    class array {
        size_t size_;
    private:
        T *host_data, *device_data; // the usual host-side and dev-side pointer pair
        bool host_dirtyYN, device_dirtyYN;
    public:
        inline void memcpyD2H() {
            host_dirtyYN = false;
            cudaMemcpy(host_data, device_data, sizeof(T) * size_, cudaMemcpyDeviceToHost);
        }

        inline void memcpyH2D() {
            device_dirtyYN = false;
            cudaMemcpy(device_data, host_data, sizeof(T) * size_, cudaMemcpyHostToDevice);
        }

        unsigned int inline size() { return size_; }

        array(size_t size, bool pinnedYN = false) : size_(size), host_dirtyYN(false), device_dirtyYN(false) {
            switch (pinnedYN) {
                case true:
                    fprintf(stderr, "Pinned memory not implemented yet, but it's easy: cudaHostAlloc(...)\n");
                    exit(1);
                    break;
                case false:
                    // init host pointer
                    host_data = new T[size_];
                    break;
            }
            // init device pointer
            cudaMalloc((void **) &device_data, sizeof(T) * size_);
            cudaDeviceSynchronize();
        }

        array &operator=(array &) noexcept = default;

        array &operator=(array &&) noexcept = default;

        inline T *host() { // return host pointer
            if (host_dirtyYN) memcpyD2H();
            device_dirtyYN = true;
            return host_data;
        }

        inline T *device() { // return device pointer
            if (device_dirtyYN) memcpyH2D();
            host_dirtyYN = true;
            return device_data;
        }

        inline void sync() { // force synchronization to undirty the buffers (not usually necessary)
            if (host_dirtyYN) memcpyD2H();
            else if (device_dirtyYN) memcpyH2D();
        }

        inline void deviceFree() {
            cudaFree(device_data);
        }
    };
}