#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "CCU.cuh" //loads Custom CUda memory wrapper

//algorithm parameters
#define POPSIZE 100
#define MAX_UPDATE 10000
#define W 0.4
#define C1 2
#define C2 2
#define SPEED_LIMIT 2

inline
float frand(float max) {
    return ((float) rand() / (float) RAND_MAX) * max;
}

// checks cudaGetLastError and prints the associated message if any found, otherwise does nothing
inline void cudaShowErrors() {
    cudaError_t error = cudaGetLastError();
    if (error != cudaSuccess) {
        printf("CUDA error: %s\n", cudaGetErrorString(error));
        exit(-1);
    }
}


__global__
void set_global_best(float *X, float *Y, float *Gx, float *Gy, float *Gf, float *F) {
    int ID = blockIdx.x;
    if (ID == 0) {
        for (int i = 0; i < POPSIZE; i++) {
            if (F[i] < Gf[0]) {
                Gx[0] = X[i];
                Gy[0] = Y[i];
                Gf[0] = F[i];
            }
        }
    }
}


__global__
void update_particles(float *X, float *Y, float *Vx, float *Vy, float *Px, float *Py, float *Gx, float *Gy, float *Gf,
                      float *F) {
    //collect block ID
    int ID = blockIdx.x;
    //printf("blockIdx.x = %d\n", ID);
    //ensure blockID is within the array bounds
    if (ID < POPSIZE) {
        //update velocity
        Vx[ID] = W * Vx[ID] + C1 * (Px[ID] - X[ID]) + C2 * (Gx[0] - X[ID]);
        if (Vx[ID] > SPEED_LIMIT) { Vx[ID] = SPEED_LIMIT; }
        if (Vx[ID] < -SPEED_LIMIT) { Vx[ID] = -SPEED_LIMIT; }
        Vy[ID] = W * Vy[ID] + C1 * (Py[ID] - Y[ID]) + C2 * (Gy[0] - Y[ID]);
        if (Vy[ID] > SPEED_LIMIT) { Vy[ID] = SPEED_LIMIT; }
        if (Vy[ID] < -SPEED_LIMIT) { Vy[ID] = -SPEED_LIMIT; }
        //update position
        X[ID] = X[ID] + Vx[ID];
        Y[ID] = Y[ID] + Vy[ID];
        //update personal best and fitness
        float tempFit = (X[ID] * X[ID]) + (Y[ID] * Y[ID]);
        if (tempFit < F[ID]) {
            Px[ID] = X[ID];
            Py[ID] = Y[ID];
        }
        F[ID] = tempFit;
    }
}

int newMain() {
    //seed random number generator with system time (second)
    srand(time(0));

    //attempt to grab a card
    int deviceCount;
    cudaGetDeviceCount(&deviceCount);
    printf("capturing device #%d\n", deviceCount);
    cudaSetDevice(deviceCount - 1);
    cudaShowErrors();

    //create host and device arrays with CCU
    ccu::array<float> X(POPSIZE);
    ccu::array<float> Y(POPSIZE);
    ccu::array<float> Vx(POPSIZE);
    ccu::array<float> Vy(POPSIZE);
    ccu::array<float> Px(POPSIZE);
    ccu::array<float> Py(POPSIZE);
    ccu::array<float> Gx(1);
    ccu::array<float> Gy(1);
    ccu::array<float> Gf(1);
    ccu::array<float> F(POPSIZE);

    //init host data
    Gx.host()[0] = -1;
    Gy.host()[0] = -1;
    Gf.host()[0] = 1e50;
    for (int i = 0; i < POPSIZE; i++) {
        X.host()[i] = frand(1);
        Y.host()[i] = frand(1);
        Vx.host()[i] = frand(1);
        Vy.host()[i] = frand(1);
        Px.host()[i] = X.host()[i];
        Py.host()[i] = Y.host()[i];
        F.host()[i] = (X.host()[i] * X.host()[i]) + (Y.host()[i] * Y.host()[i]); //x^2 + y^2
        if (F.host()[i] < Gf.host()[0]) {
            Gx.host()[0] = X.host()[i];
            Gy.host()[0] = Y.host()[i];
            Gf.host()[0] = F.host()[i];
        }
    }

    //run simulation
    for (int update = 0; update < MAX_UPDATE; update++) {
        update_particles << < POPSIZE, 1 >> > (X.device(), Y.device(), Vx.device(), Vy.device(), Px.device(),
                Py.device(), Gx.device(), Gy.device(), Gf.device(), F.device());
        cudaShowErrors();
        cudaDeviceSynchronize();

        set_global_best << < 1, 1 >> > (X.device(), Y.device(), Gx.device(), Gy.device(), Gf.device(), F.device());
        cudaShowErrors();
        cudaDeviceSynchronize();
    }

    //print output
    for (int i = 0; i < POPSIZE; i++) {
        printf("Par: %d Fit: %f @ (%f,%f) + <%f,%f> | Best: (%f,%f)\n", i, F.host()[i], X.host()[i], Y.host()[i],
               Vx.host()[i], Vy.host()[i], Px.host()[i], Py.host()[i]);
    }
    printf("Global best: %f @ (%f,%f)\n", Gf.host()[0], Gx.host()[0], Gy.host()[0]);

    //free up device memory
    X.deviceFree();
    Y.deviceFree();
    Vx.deviceFree();
    Vy.deviceFree();
    Px.deviceFree();
    Py.deviceFree();
    Gx.deviceFree();
    Gy.deviceFree();
    Gf.deviceFree();
    F.deviceFree();

    return 0;
}


int main() {

    return newMain();
    /*
    //seed random number generator with system time (second)
    srand(time(0));
    //attempt to grab a card
    int deviceCount;
    cudaGetDeviceCount(&deviceCount);
    printf("capturing device #%d\n", deviceCount);
    cudaSetDevice(deviceCount - 1);
    cudaShowErrors();
    //create host arrays
    float h_X[POPSIZE], h_Y[POPSIZE], h_Vx[POPSIZE], h_Vy[POPSIZE], h_Px[POPSIZE], h_Py[POPSIZE], h_Gx[1], h_Gy[1], h_Gf[1], h_F[POPSIZE];
    //create device arrays
    float *d_X, *d_Y, *d_Vx, *d_Vy, *d_Px, *d_Py, *d_Gx, *d_Gy, *d_Gf, *d_F;
    cudaMalloc((void **) &d_X, POPSIZE * sizeof(float));
    cudaMalloc((void **) &d_Y, POPSIZE * sizeof(float));
    cudaMalloc((void **) &d_Vx, POPSIZE * sizeof(float));
    cudaMalloc((void **) &d_Vy, POPSIZE * sizeof(float));
    cudaMalloc((void **) &d_Px, POPSIZE * sizeof(float));
    cudaMalloc((void **) &d_Py, POPSIZE * sizeof(float));
    cudaMalloc((void **) &d_Gx, 1 * sizeof(float));
    cudaMalloc((void **) &d_Gy, 1 * sizeof(float));
    cudaMalloc((void **) &d_Gf, 1 * sizeof(float));
    cudaMalloc((void **) &d_F, POPSIZE * sizeof(float));
    //init host data
    h_Gx[0] = -1;
    h_Gy[0] = -1;
    h_Gf[0] = 1e50;
    for (int i = 0; i < POPSIZE; i++) {
        h_X[i] = frand(1);
        h_Y[i] = frand(1);
        h_Vx[i] = frand(1);
        h_Vy[i] = frand(1);
        h_Px[i] = h_X[i];
        h_Py[i] = h_Y[i];
        h_F[i] = (h_X[i] * h_X[i]) + (h_Y[i] * h_Y[i]); //x^2 + y^2
        if (h_F[i] < h_Gf[0]) {
            h_Gx[0] = h_X[i];
            h_Gy[0] = h_Y[i];
            h_Gf[0] = h_F[i];
        }
    }
    //copy input data to arrays on GPU
    cudaMemcpy(d_X, h_X, POPSIZE * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Y, h_Y, POPSIZE * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Vx, h_Vx, POPSIZE * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Vy, h_Vy, POPSIZE * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Px, h_Px, POPSIZE * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Py, h_Py, POPSIZE * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Gx, h_Gx, 1 * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Gy, h_Gy, 1 * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_Gf, h_Gf, 1 * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_F, h_F, POPSIZE * sizeof(float), cudaMemcpyHostToDevice);
    //run simulation
    for (int update = 0; update < MAX_UPDATE; update++) {
        //printf("update: %d\n", update);
        update_particles << < POPSIZE, 1 >> > (d_X, d_Y, d_Vx, d_Vy, d_Px, d_Py, d_Gx, d_Gy, d_Gf, d_F);
        cudaShowErrors();
        cudaDeviceSynchronize();
        set_global_best << < 1, 1 >> > (d_X, d_Y, d_Gx, d_Gy, d_Gf, d_F);
        cudaShowErrors();
        cudaDeviceSynchronize();
    }
    //copy output from GPU back to CPU
    cudaMemcpy(h_X, d_X, POPSIZE * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_Y, d_Y, POPSIZE * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_Vx, d_Vx, POPSIZE * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_Vy, d_Vy, POPSIZE * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_Px, d_Px, POPSIZE * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_Py, d_Py, POPSIZE * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_Gx, d_Gx, 1 * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_Gy, d_Gy, 1 * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_Gf, d_Gf, 1 * sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_F, d_F, POPSIZE * sizeof(float), cudaMemcpyDeviceToHost);
    //print output
    for (int i = 0; i < POPSIZE; i++) {
        printf("Par: %d Fit: %f @ (%f,%f) + <%f,%f> | Best: (%f,%f)\n", i, h_F[i], h_X[i], h_Y[i], h_Vx[i], h_Vy[i],
               h_Px[i], h_Py[i]);
    }
    printf("Global best: %f @ (%f,%f)\n", h_Gf[0], h_Gx[0], h_Gy[0]);
    //free up device memory
    cudaFree(d_X);
    cudaFree(d_Y);
    cudaFree(d_Vx);
    cudaFree(d_Vy);
    cudaFree(d_Px);
    cudaFree(d_Py);
    cudaFree(d_Gx);
    cudaFree(d_Gy);
    cudaFree(d_Gf);
    cudaFree(d_F);

    return 0;
     */
}