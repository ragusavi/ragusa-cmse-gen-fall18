
#include <random>
#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

std::random_device rd;
std::mt19937 e2(rd());
std::uniform_real_distribution<> dist(0,1);

double Gf, Gx, Gy;
const double max_speed = 0.1;

class Particle{
private:
	double X,Y,Vx,Vy,Px,Py,F;
	double w = 0.8;
	double c1 = 2;
	double c2 = 2;
	double fitness(double x, double y){
		return (x*x) + (y*y);
	}	
public:
	Particle():X(2*dist(e2)-1), Y(2*dist(e2)-1), Vx(2*dist(e2)-1), Vy(2*dist(e2)-1){
		Px = X;
		Py = Y;
		F = fitness(X,Y);
	}
	
	void update_velocity(){
		Vx = w*Vx + c1*dist(e2)*(Px-X) + c2*dist(e2)*(Gx-X);
		if (Vx > max_speed){Vx = max_speed;}
		if (Vx < (-1)*max_speed){Vx = (-1)*max_speed;}
		Vy = w*Vy + c1*dist(e2)*(Py-Y) + c2*dist(e2)*(Gy-Y);		
		if (Vy > max_speed){Vy = max_speed;}
		if (Vy < (-1)*max_speed){Vy = (-1)*max_speed;}
	}

	void update_position(){
		X = X + Vx;
		Y = Y + Vy;
	}
	
	void update_best(){
		if (fitness(X,Y) < F){
			Px = X;
			Py = Y;
		}
		F = fitness(X,Y);
	}

	double get_fitness(){
		return F;
	}

	double get_X(){
		return X;
	}

	double get_Y(){
		return Y;
	}

	std::string print(){
	
		//std::cout << X << "," << Y << "," << Vx << "," << Vy << std::endl;
		std::ostringstream oss;
		oss << X << "," << Y << "," << Vx << "," << Vy << "|";
		return oss.str();
	}
};

unsigned long pop_size = 50;
unsigned long max_time = 1000;

int main(){
	std::ofstream outputFile;
	outputFile.open("PSO_data.csv");
	
	Gf = 1e300;
	std::vector<Particle> population(pop_size);
	
	for (auto& par:population){
		if (par.get_fitness() < Gf){
			Gf = par.get_fitness();
			Gx = par.get_X();
			Gy = par.get_Y();
		}
	}	

	for (unsigned long update = 0; update < max_time; update++){
		//std::cout << "Update: " << update << " Min: " << Gf << std::endl;
		for (auto& par:population){
			par.update_velocity();	
			//std:: cout << par.print();
			outputFile << par.print();
			par.update_position();
			par.update_best();
		}
		//std::cout << ";";//std::endl;
		outputFile << ";";//std::endl;
		
		for (auto& par:population){
        	        if (par.get_fitness() < Gf){
                	        Gf = par.get_fitness();
                        	Gx = par.get_X();
                       		Gy = par.get_Y();
               		}
        	}
	}
	
	outputFile.close();

}
